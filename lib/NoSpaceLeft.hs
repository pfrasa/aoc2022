module NoSpaceLeft (challengePair) where

import Control.Arrow ((>>>))
import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.Functor (void)
import Data.List (foldl')
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Data.Void (Void)
import Text.Megaparsec
import Text.Megaparsec.Char
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

solve1' :: ByteString -> String
solve1' =
  getFileInfosFromInput
  >>> M.elems
  >>> filter isBigEnoughDir
  >>> fmap snd
  >>> sum
  >>> show

  where
    isBigEnoughDir :: FileInfo -> Bool
    isBigEnoughDir (TypeDir, size) = size <= 100000
    isBigEnoughDir _ = False

solve2' :: ByteString -> String
solve2' input =
  let fileInfos = getFileInfosFromInput input
      totalUsed = maybe 0 snd $ M.lookup [] fileInfos
      totalUnused = 70000000 - totalUsed
      toRemove = 30000000 - totalUnused
      dirSizes = snd <$> filter (\x -> fst x == TypeDir) (M.elems fileInfos)
  in  show $ minimum $ filter (>= toRemove) dirSizes

getFileInfosFromInput :: ByteString -> FileInfos
getFileInfosFromInput =
  parseInput
  >>> readTree
  >>> ([],)
  >>> getFileInfos M.empty

data Command = Cd FolderId | Ls [FileDesc] deriving (Eq, Show)
data FolderId = Root | Parent | Child Text deriving (Eq, Show)
data FileDesc = Dir Text | Leaf Text Integer deriving (Eq, Show)

type Parser = Parsec Void Text

parseInput :: ByteString -> [Command]
parseInput = either (error "Bad parse!") id . runParser inputParser "" . decodeUtf8

inputParser :: Parser [Command]
inputParser = many commandParser
  where
    commandParser :: Parser Command
    commandParser = try cdParser <|> lsParser

    cdParser :: Parser Command
    cdParser = do
      void $ string "$ cd "
      folderId <- choice [ Root <$ char '/'
                         , Parent <$ string ".."
                         , Child . T.pack <$> some alphaNumChar
                         ]
      void $ optional $ char '\n'
      return $ Cd folderId

    lsParser :: Parser Command
    lsParser = do
      void $ string "$ ls\n"
      fileDescs <- many fileDescParser
      void $ optional $ char '\n'
      return $ Ls fileDescs

    fileDescParser :: Parser FileDesc
    fileDescParser = try dirParser <|> leafParser

    dirParser :: Parser FileDesc
    dirParser = do
      void $ string "dir "
      name <- T.pack <$> some alphaNumChar
      void $ optional $ char '\n'
      return $ Dir name

    leafParser :: Parser FileDesc
    leafParser = do
      size <- read <$> some digitChar
      space1
      name <- T.pack <$> some (alphaNumChar <|> punctuationChar)
      void $ optional $ char '\n'
      return $ Leaf name size

type Path = [Text]
data FileNode = DirNode (Map Text FileNode) | LeafNode Integer deriving (Eq, Show)
data ReadState = ReadState { root :: FileNode, currentPath :: Path } deriving (Eq, Show)

readTree :: [Command] -> FileNode
readTree = root . foldl' processCommand startState
  where
    startState :: ReadState
    startState = ReadState { root = DirNode M.empty, currentPath = [] }

    processCommand :: ReadState -> Command -> ReadState
    processCommand state command =
      case command of
        Cd Root -> state { currentPath = [] }
        Cd Parent -> state { currentPath = tail state.currentPath }
        Cd (Child subdir) -> state { currentPath = subdir : (state.currentPath) }
        Ls fileDescs -> foldl' processFileDesc state fileDescs

    processFileDesc :: ReadState -> FileDesc -> ReadState
    processFileDesc state fileDesc =
      case fileDesc of
        Leaf fileName size ->
          let newNode = LeafNode size
              newRoot = insertNode (reverse state.currentPath) (fileName, newNode) state.root
          in  state { root = newRoot }
        _ -> state

    insertNode :: Path -> (Text, FileNode) -> FileNode -> FileNode
    insertNode path (fileName, newNode) (DirNode children) =
      case path of
        [] -> DirNode (M.insert fileName newNode children)
        p:ps ->
          let child = fromMaybe (DirNode M.empty) $ M.lookup p children
              updatedChild = insertNode ps (fileName, newNode) child
          in  DirNode (M.insert p updatedChild children)
    insertNode _ _ _ = error "invalid state!"

data FileType = TypeDir | TypeFile deriving (Eq, Show)
type FileInfo = (FileType, Integer)
type FileInfos = Map Path FileInfo

getFileInfos :: FileInfos -> (Path, FileNode) -> FileInfos
getFileInfos sizes (path, DirNode children) =
  let absPathChildren = M.mapWithKey (\childRelPath child -> (childRelPath : path, child)) children
      sizesWithChildren = foldl' getFileInfos sizes absPathChildren
      childrenPaths = fst <$> M.elems absPathChildren
      childrenSizes = snd <$> mapMaybe (`M.lookup` sizesWithChildren) childrenPaths
      thisSize = sum childrenSizes
  in  M.insert path (TypeDir, thisSize) sizesWithChildren
getFileInfos sizes (path, LeafNode size) = M.insert path (TypeFile, size) sizes

testData' :: ByteString
testData' = $(embedFile "data/test/7-NoSpaceLeft.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/7-NoSpaceLeft.txt")