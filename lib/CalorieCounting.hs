module CalorieCounting 
  ( challengePair
  ) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.List (sort)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

type Inventory = [[Int]]

solve1' :: ByteString -> String
solve1' = show . findMax . parseInventory

solve2' :: ByteString -> String
solve2' = show . findMax3 . parseInventory

parseInventory :: ByteString -> Inventory
parseInventory = parse' . decodeUtf8
  where
    parse' :: Text -> Inventory
    parse' = fmap (fmap (read . T.unpack) . filter (not . T.null) . T.splitOn "\n") . T.splitOn "\n\n"

findMax :: Inventory -> Int
findMax = maximum . fmap sum

findMax3 :: Inventory -> Int
findMax3 = sum . take 3 . reverse . sort . fmap sum

testData' :: ByteString
testData' = $(embedFile "data/test/1-Calories.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/1-Calories.txt")

