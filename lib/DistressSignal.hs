module DistressSignal (challengePair) where

import Control.Arrow ((>>>))
import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.List (sortBy, elemIndex)
import Data.Maybe (mapMaybe, listToMaybe)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.Void (Void)
import Text.Megaparsec
import Text.Megaparsec.Char
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

solve1' :: ByteString -> String
solve1' = show . sum . fmap fst . filter (\x -> snd x == Just True) . fmap (fmap (uncurry compareElem)) . zip ([1..] :: [Int]) . parseInput

solve2' :: ByteString -> String
solve2' =
  parseInput
  >>> combinePackets
  >>> (dividers ++)
  >>> sortBy (fmap toOrdering . compareElem)
  >>> (\packets -> mapMaybe (`elemIndex` packets) dividers)
  >>> fmap (+1)
  >>> product
  >>> show
  where
    toOrdering :: Maybe Bool -> Ordering
    toOrdering (Just True) = LT
    toOrdering (Just False) = GT
    toOrdering Nothing = EQ

data Element = Scalar Int | Sequence [Element] deriving (Eq, Show)
type Packet = Element

dividers :: [Packet]
dividers = [Sequence [Sequence [Scalar 2]], Sequence [Sequence [Scalar 6]]]

type Parser = Parsec Void Text

parseInput :: ByteString -> [(Packet, Packet)]
parseInput = either (error "bad parse!") id . runParser inputParser "" . decodeUtf8

inputParser :: Parser [(Packet, Packet)]
inputParser = pairParser `sepBy` string "\n\n"
  where
    pairParser :: Parser (Packet, Packet)
    pairParser = (,) <$> (elemParser <* char '\n') <*> elemParser

    elemParser :: Parser Element
    elemParser = try (Scalar . read <$> some numberChar) <|> seqParser

    seqParser :: Parser Element
    seqParser = Sequence <$> (char '[' *> (elemParser `sepBy` char ',') <* char ']')

combinePackets :: [(Packet, Packet)] -> [Packet]
combinePackets pairs = pairs >>= \(p1, p2) -> [p1, p2]

compareElem :: Element -> Element -> Maybe Bool
compareElem (Scalar m) (Scalar n)
  | m < n = Just True
  | m > n = Just False
  | otherwise = Nothing
compareElem (Sequence xs) (Sequence ys) =
  compareLengths $ listToMaybe $ mapMaybe (uncurry compareElem) (zip xs ys)
  where
    compareLengths :: Maybe Bool -> Maybe Bool
    compareLengths (Just res) = Just res
    compareLengths Nothing
      | length xs < length ys = Just True
      | length xs > length ys = Just False
      | otherwise = Nothing
compareElem (Scalar x) (Sequence ys) = compareElem (Sequence [Scalar x]) (Sequence ys)
compareElem (Sequence xs) (Scalar y) = compareElem (Sequence xs) (Sequence [Scalar y])

testData' :: ByteString
testData' = $(embedFile "data/test/13-Packets.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/13-Packets.txt")