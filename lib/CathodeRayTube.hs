module CathodeRayTube (challengePair) where

import Data.ByteString (ByteString)
import Data.List (foldl')
import Data.List.Split (chunksOf)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Data.FileEmbed (embedFile)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

solve1' :: ByteString -> String
solve1' = show . signalStrengthSum . runProgram initialState . parseInput

solve2' :: ByteString -> String
solve2' = unlines . chunksOf 40 . reverse . screen . runProgram initialState . parseInput

data Instruction = Noop | Addx Int deriving (Eq, Show)

parseInput :: ByteString -> [Instruction]
parseInput = fmap parseLine . T.lines . decodeUtf8
  where
    parseLine :: Text -> Instruction
    parseLine = parseComponents . T.words

    parseComponents :: [Text] -> Instruction
    parseComponents ["noop"] = Noop
    parseComponents ["addx", val] = Addx $ read $ T.unpack val
    parseComponents cs = error $ "Invalid input: " <> show cs

data State = State { xReg :: Int, cycle :: Int, signalStrengthSum :: Int, screen :: [Char] } deriving (Eq, Show)

initialState :: State
initialState = State { xReg = 1, cycle = 1, signalStrengthSum = 0, screen = [] }

runProgram :: State -> [Instruction] -> State
runProgram = foldl' runInstruction

runInstruction :: State -> Instruction -> State
runInstruction state instruction = foldl' updateCycle state registerUpdates
  where
    registerUpdates :: [Int -> Int]
    registerUpdates =
      case instruction of
        Noop -> [id]
        Addx n -> [id, (+ n)]

    updateCycle :: State -> (Int -> Int) -> State
    updateCycle state' registerUpdate =
      state { xReg = registerUpdate state'.xReg
            , cycle = state'.cycle + 1
            , signalStrengthSum = updateSignalStrengthSum state'
            , screen = drawPixel state' : state'.screen }

    updateSignalStrengthSum :: State -> Int
    updateSignalStrengthSum state' =
      if shouldGetSignalStrength state'.cycle then
        state'.signalStrengthSum + state'.cycle * state'.xReg
      else
        state'.signalStrengthSum

    shouldGetSignalStrength :: Int -> Bool
    shouldGetSignalStrength n = (n - 20) `mod` 40 == 0

    drawPixel :: State -> Char
    drawPixel state'
      | abs (horizontalPosition state'.cycle - state'.xReg) <= 1 = '#'
      | otherwise = '.'

    horizontalPosition :: Int -> Int
    horizontalPosition cycle' = (cycle' - 1) `mod` 40

testData' :: ByteString
testData' = $(embedFile "data/test/10-Instructions.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/10-Instructions.txt")