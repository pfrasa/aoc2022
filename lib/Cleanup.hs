module Cleanup
  ( challengePair
  ) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

type Sections = Set Int
type Pair = (Sections, Sections)

solve1' :: ByteString -> String
solve1' = show . solve . parsePairs
  where
    solve :: [Pair] -> Int
    solve = length . filter hasInclusion

    hasInclusion :: Pair -> Bool
    hasInclusion (s1, s2) =
      Set.isSubsetOf s1 s2 || Set.isSubsetOf s2 s1

solve2' :: ByteString -> String
solve2' = show . solve . parsePairs
  where
    solve :: [Pair] -> Int
    solve = length . filter hasOverlap

    hasOverlap :: Pair -> Bool
    hasOverlap (s1, s2) = not $ Set.null $ Set.intersection s1 s2

parsePairs :: ByteString -> [Pair]
parsePairs = parse' . decodeUtf8
  where
    parse' :: Text -> [Pair]
    parse' = fmap parseSingle . filter (not . T.null) . T.splitOn "\n"

    parseSingle :: Text -> Pair
    parseSingle = toPair . fmap parseSections . T.splitOn ","

    parseSections :: Text -> Sections
    parseSections = toSections . fmap (read . T.unpack) . T.splitOn "-"

    toSections :: [Int] -> Sections
    toSections [start, end] = Set.fromList [start..end]
    toSections x = error $ "Invalid sections: " <> show x

    toPair :: [Sections] -> Pair
    toPair [s1, s2] = (s1, s2)
    toPair x = error $ "Invalid sections pair: " <> show x

testData' :: ByteString
testData' = $(embedFile "data/test/4-Cleanup.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/4-Cleanup.txt")