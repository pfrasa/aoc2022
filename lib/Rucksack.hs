module Rucksack
  ( challengePair
  ) where

import Data.ByteString (ByteString)
import Data.Char (ord)
import Data.FileEmbed (embedFile)
import Data.List (find, nub)
import Data.List.Split (chunksOf)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

newtype Item = Item Char deriving (Eq, Show)
newtype Rucksack = Rucksack ([Item], [Item]) deriving (Eq, Show)

solve1' :: ByteString -> String
solve1' input = show . sum $ do
  rucksack <- parseRucksacks input
  common <- findCommon rucksack
  return $ priority common

solve2' :: ByteString -> String
solve2' = show . sum . fmap groupPriorityScore . chunksOf 3 . parseRucksacks
  where
    groupPriorityScore :: [Rucksack] -> Int
    groupPriorityScore [Rucksack (i1a, i1b), Rucksack (i2a, i2b), Rucksack (i3a, i3b)] =
      let i1 = nub $ i1a ++ i1b
          i2 = nub $ i2a ++ i2b
          i3 = nub $ i3a ++ i3b
          commonItem = find (\x -> x `elem` i2 && x `elem` i3) i1
      in  maybe 0 priority commonItem
    groupPriorityScore _ = error "invalid state"

parseRucksacks :: ByteString -> [Rucksack]
parseRucksacks = parse' . decodeUtf8
  where
    parse' :: Text -> [Rucksack]
    parse' = fmap parseRucksack . filter (not . T.null) . T.splitOn "\n"

parseRucksack :: Text -> Rucksack
parseRucksack input =
  let items = parseItems input
      compartmentLength = length items `div` 2
  in  Rucksack ( take compartmentLength items
               , take compartmentLength (drop compartmentLength items)
               )

parseItems :: Text -> [Item]
parseItems = fmap Item . T.unpack

findCommon :: Rucksack -> [Item]
findCommon (Rucksack (comp1, comp2)) = nub $ filter (`elem` comp2) comp1

priority :: Item -> Int
priority (Item c)
  | ord c >= ord 'a' && ord c <= ord 'z' = ord c - ord 'a' + 1
  | ord c >= ord 'A' && ord c <= ord 'Z' = ord c - ord 'A' + 27
  | otherwise = error $ "Invalid char " <> show c

testData' :: ByteString
testData' = $(embedFile "data/test/3-Rucksack.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/3-Rucksack.txt")