module RopeBridge (challengePair, debugState) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.List (elemIndex, foldl')
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair (solve' 2) (solve' 10) testData' challengeData'

solve' :: Int -> ByteString -> String
solve' len = show . Set.size . (\(State _ visited) -> visited) . foldl' executeMove (initialState len) . parseInput

data Direction = L | R | U | D deriving (Eq, Show, Read)
data Move = Move Direction Int deriving (Eq, Show)

parseInput :: ByteString -> [Move]
parseInput = parse . decodeUtf8
  where
    parse :: Text -> [Move]
    parse = fmap parseLine . filter (not . T.null) . T.splitOn "\n"

    parseLine :: Text -> Move
    parseLine = parseComponents . fmap T.unpack . T.splitOn " "

    parseComponents :: [String] -> Move
    parseComponents [dir, amount] = Move (read dir) (read amount)
    parseComponents cs = error $ "Invalid input: " <> show cs

type Pos = (Int, Int)
newtype Rope = Rope [Pos] deriving (Eq, Show)
type Visited = Set Pos
data State = State Rope Visited deriving (Eq, Show)

initialState :: Int -> State
initialState len = State (Rope (replicate len (0, 0))) (Set.fromList [(0, 0)])

executeMove :: State -> Move -> State
executeMove state (Move dir amount) = foldl' moveTo state $ replicate amount dir

moveTo :: State -> Direction -> State
moveTo (State (Rope ((hi, hj) : tails)) visited) dir =
  let (hi', hj') = case dir of
                        L -> (hi, hj - 1)
                        R -> (hi, hj + 1)
                        U -> (hi - 1, hj)
                        D -> (hi + 1, hj)
      newTails = reverse $ foldl' (doFold (hi', hj')) [] tails
      newVisited = Set.insert (last newTails) visited
      newRope = Rope ((hi', hj') : newTails)
  in  State newRope newVisited
  where
    doFold :: Pos -> [Pos] -> Pos -> [Pos]
    doFold (hi', hj') [] t = [newTailPos (hi', hj') t]
    doFold _ (t1 : ts) t = newTailPos t1 t : t1 : ts

    newTailPos :: Pos -> Pos -> Pos
    newTailPos (prevI, prevJ) (i, j)
      | prevI == i - 2 && prevJ == j - 2 = (i - 1, j - 1)
      | prevI == i - 2 && prevJ == j + 2 = (i - 1, j + 1)
      | prevI == i + 2 && prevJ == j - 2 = (i + 1, j - 1)
      | prevI == i + 2 && prevJ == j + 2 = (i + 1, j + 1)
      | prevI == i - 2 = (i - 1, prevJ)
      | prevJ == j + 2 = (prevI, j + 1)
      | prevI == i + 2 = (i + 1, prevJ)
      | prevJ == j - 2 = (prevI, j - 1)
      | otherwise = (i, j)

moveTo state _ = state

debugState :: State -> IO ()
debugState (State rope visited) = putStrLn $ drawRope rope <> "\n\n" <> show visited <> "\n\n"

drawRope :: Rope -> String
drawRope (Rope knots) = unlines $ drawRow <$> [-10..10]
  where
    drawRow :: Int -> String
    drawRow ri = drawPos <$> zip (repeat ri) [-10..10]

    drawPos :: Pos -> Char
    drawPos pos =
      case elemIndex pos knots of
        Nothing -> if pos == (0, 0) then 's' else '.'
        Just i -> if i == 0 then 'H' else head (show i)

testData' :: ByteString
testData' = $(embedFile "data/test/9-Rope.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/9-Rope.txt")