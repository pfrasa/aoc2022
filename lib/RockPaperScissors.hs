module RockPaperScissors
  ( challengePair
  ) where

import Prelude hiding (round)
import Data.ByteString (ByteString)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Data.FileEmbed (embedFile)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

data Move = Rock | Paper | Scissors deriving (Eq, Show)
data Round = Round { opponent :: Move, you :: Move } deriving (Eq, Show)
data Outcome = Win | Loss | Draw deriving (Eq, Show)
data Strategy = Strategy { opponent :: Move, outcome :: Outcome }

solve1' :: ByteString -> String
solve1' = show . totalScore . parseRounds 

solve2' :: ByteString -> String
solve2' = show . totalScore . fmap strategyToRound . parseStrategies

parseRounds :: ByteString -> [Round]
parseRounds = parseInput parseComponents
  where
    parseComponents :: [Text] -> Round
    parseComponents [tOpponent, tYou] = Round (parseOpponent tOpponent) (parseYou tYou)
    parseComponents input = error $ "bad input: " <> show input

parseStrategies :: ByteString -> [Strategy]
parseStrategies = parseInput parseComponents
  where
    parseComponents :: [Text] -> Strategy
    parseComponents [tOpponent, tYou] = Strategy (parseOpponent tOpponent) (parseOutcome tYou)
    parseComponents input = error $ "bad input: " <> show input

parseInput :: forall a. ([Text] -> a) -> ByteString -> [a]
parseInput parseComponents = parse' . decodeUtf8
  where
    parse' :: Text -> [a]
    parse' = fmap parseLine . filter (not . T.null) . T.splitOn "\n"

    parseLine :: Text -> a
    parseLine = parseComponents . T.splitOn " "

parseOpponent :: Text -> Move
parseOpponent "A" = Rock
parseOpponent "B" = Paper
parseOpponent "C" = Scissors
parseOpponent input = error $ "bad input: " <> T.unpack input

parseYou :: Text -> Move
parseYou "X" = Rock
parseYou "Y" = Paper
parseYou "Z" = Scissors
parseYou input = error $ "bad input: " <> T.unpack input

parseOutcome :: Text -> Outcome
parseOutcome "X" = Loss
parseOutcome "Y" = Draw
parseOutcome "Z" = Win
parseOutcome input = error $ "bad input: " <> T.unpack input

strategyToRound :: Strategy -> Round
strategyToRound strategy = Round strategy.opponent determineMove
  where
    determineMove :: Move
    determineMove =
      case (strategy.opponent, strategy.outcome) of
        (Rock, Win) -> Paper
        (Rock, Loss) -> Scissors
        (Rock, Draw) -> Rock
        (Paper, Win) -> Scissors
        (Paper, Loss) -> Rock
        (Paper, Draw) -> Paper
        (Scissors, Win) -> Rock
        (Scissors, Loss) -> Paper
        (Scissors, Draw) -> Scissors

determineOutcome :: Round -> Outcome
determineOutcome round =
  case (round.opponent, round.you) of
    (Rock, Rock) -> Draw
    (Rock, Paper) -> Win
    (Rock, Scissors) -> Loss
    (Paper, Rock) -> Loss
    (Paper, Paper) -> Draw
    (Paper, Scissors) -> Win
    (Scissors, Rock) -> Win
    (Scissors, Paper) -> Loss
    (Scissors, Scissors) -> Draw

roundScore :: Round -> Int
roundScore round = moveScore round.you + outcomeScore (determineOutcome round)
  where
    moveScore :: Move -> Int
    moveScore Rock = 1
    moveScore Paper = 2
    moveScore Scissors = 3

    outcomeScore :: Outcome -> Int
    outcomeScore Win = 6
    outcomeScore Loss = 0
    outcomeScore Draw = 3

totalScore :: [Round] -> Int
totalScore = sum . fmap roundScore

testData' :: ByteString
testData' = $(embedFile "data/test/2-Strategy.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/2-Strategy.txt")