module BeaconExclusion (challengePair) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.List (foldl', sort)
import Data.Maybe (mapMaybe, listToMaybe, fromJust)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Text.Regex (Regex, mkRegex, matchRegex)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

solve1' :: ByteString -> String
solve1' bs =
  let input = parseInput bs
      blocked = blockedForRow input.sensors input.rowToCheck
      rowBeaconCols = fmap x $ filter (\b -> b.y == input.rowToCheck) $ fmap closestBeacon input.sensors
      withoutBeacons = foldl' remove blocked rowBeaconCols
  in  show $ area withoutBeacons

solve2' :: ByteString -> String
solve2' bs = show $ tuningFrequency $ fromJust $ listToMaybe $ mapMaybe findInRow [minCoord..input.maxCoord]
  where
    input :: Input
    input = parseInput bs

    findInRow :: Int -> Maybe Pos
    findInRow row = flip Pos row <$> firstGap minCoord input.maxCoord (blockedForRow input.sensors row)

    minCoord, xMultiplier :: Int
    minCoord = 0
    xMultiplier = 4000000

    tuningFrequency :: Pos -> Int
    tuningFrequency pos = pos.x * xMultiplier + pos.y

data Pos = Pos { x :: Int, y :: Int } deriving (Eq, Show)
data Sensor = Sensor { pos :: Pos, closestBeacon :: Pos } deriving (Eq, Show)
data Input = Input { rowToCheck :: Int, maxCoord :: Int, sensors :: [Sensor] } deriving (Eq, Show)

parseInput :: ByteString -> Input
parseInput = parse . T.lines . decodeUtf8
  where
    parse :: [Text] -> Input
    parse (x1:x2:xs) = Input ((read . T.unpack) x1) ((read . T.unpack) x2) (fmap parseSensor xs)
    parse l = error $ "Invalid input: " <> show l

    parseSensor :: Text -> Sensor
    parseSensor = extractSensor . fmap (fmap read). matchRegex sensorRegex . T.unpack

    sensorRegex :: Regex
    sensorRegex = mkRegex "Sensor at x=(.+), y=(.+): closest beacon is at x=(.+), y=(.+)"

    extractSensor :: Maybe [Int] -> Sensor
    extractSensor (Just [sensorX, sensorY, beaconX, beaconY]) =
      Sensor (Pos sensorX sensorY) (Pos beaconX beaconY)
    extractSensor line = error $ "Invalid input: " <> show line

distance :: Pos -> Pos -> Int
distance pos1 pos2 = abs (pos1.x - pos2.x) + abs (pos1.y - pos2.y)

data Range = Range { lower :: Int, upper :: Int } deriving (Eq, Show)
instance Ord Range where
  compare r1 r2 = compare r1.lower r2.lower

newtype Blocked = Blocked [Range] deriving (Eq, Show)

blockedForRow :: [Sensor] -> Int -> Blocked
blockedForRow sensors row = foldl' merge (Blocked []) $ blockedForRowAndSensor row <$> sensors

blockedForRowAndSensor :: Int -> Sensor -> Blocked
blockedForRowAndSensor row sensor
  | radius >= 0 = Blocked [Range (sensor.pos.x-radius) (sensor.pos.x+radius)]
  | otherwise = Blocked []
  where
    radius :: Int
    radius = (distance sensor.pos sensor.closestBeacon) - abs (sensor.pos.y - row)

merge :: Blocked -> Blocked -> Blocked
merge (Blocked ranges1) (Blocked ranges2) = Blocked $ merge' (ranges1 ++ ranges2)
  where
    merge' :: [Range] -> [Range]
    merge' [] = []
    merge' (h:t) = 
      case listToMaybe (mapMaybe (mergeOne h) t) of
        Nothing -> h : merge' t
        Just (newR1, r2) -> merge' (newR1 : filter (/= r2) t)

    mergeOne :: Range -> Range -> Maybe (Range, Range)
    mergeOne r1 r2
      | r1.lower <= r2.lower && r2.lower <= r1.upper = Just (Range r1.lower (max r1.upper r2.upper), r2)
      | r2.lower <= r1.lower && r1.lower <= r2.upper = Just (Range r2.lower (max r1.upper r2.upper), r2)
      | otherwise = Nothing

remove :: Blocked -> Int -> Blocked
remove (Blocked ranges) x = Blocked $ ranges >>= remove'
  where
    remove' :: Range -> [Range]
    remove' range
      | x < range.lower || x > range.upper = [range]
      | x == range.lower && x == range.upper = []
      | x == range.lower = [Range (x+1) range.upper]
      | x == range.upper = [Range range.lower (x-1)]
      | otherwise = [Range range.lower (x-1), Range (x+1) range.upper]

area :: Blocked -> Int
area (Blocked ranges) = sum $ fmap rangeArea ranges
  where
    rangeArea :: Range -> Int
    rangeArea range = range.upper - range.lower + 1

firstGap :: Int -> Int -> Blocked -> Maybe Int
firstGap min' max' (Blocked ranges) = go $ sort ranges
  where
    go :: [Range] -> Maybe Int
    go [] = Just min'
    go (r:_)
      | r.lower > min' = Just min'
      | r.upper < max' = Just (r.upper + 1)
      | otherwise = Nothing

testData' :: ByteString
testData' = $(embedFile "data/test/15-Sensors.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/15-Sensors.txt")