module Reservoir (challengePair) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.IntMap (IntMap)
import qualified Data.IntMap as IM
import Data.List (find, foldl')
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair (solve' . parseInput) (solve' . withFloor . parseInput) testData' challengeData'

type Pos = (Int, Int)
data Square = Empty | Filled deriving (Eq, Show)
data Grid = Grid { imap :: IntMap (IntMap Square), maxRow :: Int, hasFloor :: Bool } deriving (Eq, Show)

solve' :: Grid -> String
solve' = show . subtract 1 . length . takeWhile (not . done) . iterate dropSand . newState

emptyGrid :: Grid
emptyGrid = Grid IM.empty 0 False

addToGrid :: Bool -> Grid -> Pos -> Grid
addToGrid setMax grid (col, row) =
  let colMap = fromMaybe IM.empty $ IM.lookup col grid.imap
      newColMap = IM.insert row Filled colMap
      newMax = if setMax then max grid.maxRow row else grid.maxRow
  in  grid { imap = IM.insert col newColMap grid.imap, maxRow = newMax }

getSquare :: Grid -> Pos -> Square
getSquare grid (col, row)
  | grid.hasFloor && row == grid.maxRow + 2 = Filled
  | otherwise =
    let colMap = fromMaybe IM.empty $ IM.lookup col grid.imap
    in  fromMaybe Empty $ IM.lookup row colMap

getFilledForCol :: Grid -> Int -> [Int]
getFilledForCol grid col =
  let colMap = fromMaybe IM.empty $ IM.lookup col grid.imap
  in  maybeAddFloor $ IM.keys $ IM.filter (== Filled) colMap
  where
    maybeAddFloor :: [Int] -> [Int]
    maybeAddFloor
      | grid.hasFloor = (++ [grid.maxRow + 2])
      | otherwise = id

withFloor :: Grid -> Grid
withFloor grid = grid { hasFloor = True }

type Path = (Pos, Pos)

parseInput :: ByteString -> Grid
parseInput = foldl' addPathToGrid emptyGrid . readPaths . decodeUtf8
  where
    readPaths :: Text -> [Path]
    readPaths t = T.lines t >>= readPathsForLine

    readPathsForLine :: Text -> [Path]
    readPathsForLine = (\p -> zip p (tail p)) . fmap (readPos . T.splitOn ",") . T.splitOn " -> "

    readPos :: [Text] -> Pos
    readPos [x, y] = (read (T.unpack x), read (T.unpack y))
    readPos cs = error $ "invalid input: " <> show cs

    addPathToGrid :: Grid -> Path -> Grid
    addPathToGrid grid path = foldl' (addToGrid True) grid (pathToPos path)

    pathToPos :: Path -> [Pos]
    pathToPos path@((x1, y1), (x2, y2))
      | x1 == x2 && y1 <= y2 = (x1,) <$> [y1..y2]
      | x1 == x2 && y2 <= y1 = (x1,) <$> [y2..y1]
      | y1 == y2 && x1 <= x2 = (,y1) <$> [x1..x2]
      | y1 == y2 && x2 <= x1 = (,y1) <$> [x2..x1]
      | otherwise = error $ "bad input: " <> show path

data State = State { grid :: Grid, done :: Bool } deriving (Eq, Show)

newState :: Grid -> State
newState grid = State grid False

dropSand :: State -> State
dropSand state
  | state.done = state
  | getSquare state.grid (500, 0) == Filled = state { done = True }
  | otherwise = dropFrom (500, 0)
  where
    dropFrom :: Pos -> State
    dropFrom (col, row) =
      case find (> row) (getFilledForCol state.grid col) of
        Nothing -> state { done = True }
        Just stopRow ->
          if getSquare state.grid (col - 1, stopRow) == Empty then
            dropFrom (col - 1, stopRow)
          else if getSquare state.grid (col + 1, stopRow) == Empty then
            dropFrom (col + 1, stopRow)
          else
            state { grid = addToGrid False state.grid (col, stopRow - 1) }

testData' :: ByteString
testData' = $(embedFile "data/test/14-Paths.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/14-Paths.txt")