module Tuning (challengePair) where

import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.List (find, zip5, nub)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair (solve' findMarker) (solve' findMessageStart) testData' challengeData'

solve' :: ([Char] -> Int) -> ByteString -> String
solve' finder = show . finder . T.unpack . decodeUtf8

findMarker :: [Char] -> Int
findMarker chars = maybe 0 (\(i, _, _, _, _) -> i) $ find isMarker windows
  where
    windows :: [(Int, Char, Char, Char, Char)]
    windows = zip5 [4..] chars (tail chars) (tail (tail chars)) (tail (tail (tail chars)))

    isMarker :: (Int, Char, Char, Char, Char) -> Bool
    isMarker (_, c1, c2, c3, c4) = length (nub [c1, c2, c3, c4]) == 4

findMessageStart :: [Char] -> Int
findMessageStart chars = maybe 0 fst $ find isMessageStart windows
  where
    windows :: [(Int, [Char])]
    windows = zip [14..] $ take 14 <$> allTails chars

    isMessageStart :: (Int, [Char]) -> Bool
    isMessageStart (_, c) = length (nub c) == 14

    allTails :: [Char] -> [[Char]]
    allTails = reverse . go []
      where
        go :: [[Char]] -> [Char] -> [[Char]]
        go acc [] = acc
        go acc (x:xs) = go ((x:xs):acc) xs

testData' :: ByteString
testData' = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/6-Tuning.txt")