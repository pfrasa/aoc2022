module Treehouse (challengePair) where

import Data.ByteString (ByteString)
import Data.Char (digitToInt)
import Data.FileEmbed (embedFile)
import Data.Functor ((<&>))
import Data.List (foldl')
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

solve1' :: ByteString -> String
solve1' = show . allVisible . parseInput

solve2' :: ByteString -> String
solve2' = show . highestScenicScore . parseInput

type GridMap = Map (Int, Int) Int
data Grid = Grid { gmap :: GridMap, maxX :: Int, maxY :: Int } deriving (Eq, Show)

parseInput :: ByteString -> Grid
parseInput = toGrid . parseToLists . decodeUtf8
  where
    parseToLists :: Text -> [[Int]]
    parseToLists = fmap (fmap digitToInt. T.unpack) . filter (not . T.null) . T.lines

    toGrid :: [[Int]] -> Grid
    toGrid rows =
      let map' = (foldl' addRowToGrid M.empty . zip [1..]) rows
      in  Grid map' (length (head rows)) (length rows)

    addRowToGrid :: GridMap -> (Int, [Int]) -> GridMap
    addRowToGrid grid (i, row) = foldl' addNumToGrid grid $ zip3 (repeat i) [1..] row

    addNumToGrid :: GridMap -> (Int, Int, Int) -> GridMap
    addNumToGrid grid (i, j, x) = M.insert (i, j) x grid

visible :: Grid -> (Int, Int) -> Bool
visible grid tree = visible' (getLeft grid) || visible' (getRight grid) || visible' (getUp grid) || visible' (getDown grid)
  where
    visible' :: ((Int, Int) -> [Int]) -> Bool
    visible' getNeighbours =
      let treeHeight = fromMaybe 0 $ M.lookup tree grid.gmap
      in  all (< treeHeight) (getNeighbours tree)

allVisible :: Grid -> Int
allVisible grid = (length . filter (visible grid) . M.keys . gmap) grid

scenicScore :: Grid -> (Int, Int) -> Int
scenicScore grid tree = score (getLeft grid) * score (getRight grid) * score (getUp grid) * score (getDown grid)
  where
    score :: ((Int, Int) -> [Int]) -> Int
    score getNeighbours =
      let treeHeight = fromMaybe 0 $ M.lookup tree grid.gmap
          line = takeUntil (>= treeHeight) (getNeighbours tree)
      in  length line

highestScenicScore :: Grid -> Int
highestScenicScore grid = maximum $ scenicScore grid <$> M.keys grid.gmap

takeUntil :: (a -> Bool) -> [a] -> [a]
takeUntil _ [] = []
takeUntil p (x:xs) = x : if not (p x) then takeUntil p xs
                                      else []

getLeft :: Grid -> (Int, Int) -> [Int]
getLeft grid (i, j) = reverse [1..j-1] <&> \newJ -> fromMaybe 0 (M.lookup (i, newJ) grid.gmap)

getRight :: Grid -> (Int, Int) -> [Int]
getRight grid (i, j) = [j+1..grid.maxX] <&> \newJ -> fromMaybe 0 (M.lookup (i, newJ) grid.gmap)

getUp :: Grid -> (Int, Int) -> [Int]
getUp grid (i, j) = reverse [1..i-1] <&> \newI -> fromMaybe 0 (M.lookup (newI, j) grid.gmap)

getDown :: Grid -> (Int, Int) -> [Int]
getDown grid (i, j) = [i+1..grid.maxY] <&> \newI -> fromMaybe 0 (M.lookup (newI, j) grid.gmap)

testData' :: ByteString
testData' = $(embedFile "data/test/8-Trees.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/8-Trees.txt")