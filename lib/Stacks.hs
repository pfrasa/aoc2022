module Stacks (challengePair) where

import Control.Arrow ((>>>))
import Control.Monad.State
import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.IntMap.Strict (IntMap, (!))
import qualified Data.IntMap.Strict as IM
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Text.Regex (Regex, matchRegex, mkRegex)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair (solve' executeMove1) (solve' executeMove2) testData' challengeData'

data Input = Input Stacks [Move] deriving (Eq, Show)
type Stacks = IntMap Stack
type Stack = [Crate]
type Crate = Char
data Move = Move { amount :: Int, from :: Int, to :: Int } deriving (Eq, Show)

type MoveExecutor = Move -> State Stacks ()

solve' :: MoveExecutor -> ByteString -> String
solve' executeMove = fmap head . IM.elems . processInput executeMove . parseInput

parseInput :: ByteString -> Input
parseInput = parse' . decodeUtf8
  where
    parse' :: Text -> Input
    parse' = parseParts . T.splitOn "\n\n"

    parseParts :: [Text] -> Input
    parseParts [stacks, moves] = Input (parseStacks stacks) (parseMoves moves)
    parseParts l = error $ "invalid input: " <> show l

    parseMoves :: Text -> [Move]
    parseMoves = fmap parseMove . T.lines

parseStacks :: Text -> Stacks
parseStacks =
  -- get input lines
  T.lines
  -- get input rows
  >>> T.transpose
  -- remove whitespace-only rows
  >>> filter (not . T.null . T.strip)
  -- only keep rows with the crates
  >>> zip ([1..] :: [Int])
  >>> filter (\(i, _) -> i `mod` 3 == 2)
  >>> fmap snd
  -- drop last line (stack number)
  >>> fmap (T.dropEnd 1)
  -- drop empty lines
  >>> fmap T.strip
  -- convert to stack
  >>> fmap T.unpack
  >>> zip ([1..] :: [Int])
  >>> IM.fromList

parseMove :: Text -> Move
parseMove t = extract $ matchRegex regexp (T.unpack t)
  where
    regexp :: Regex
    regexp = mkRegex "move (.+) from (.+) to (.+)"

    extract :: Maybe [String] -> Move
    extract (Just [amountS, fromS, toS]) = Move (read amountS) (read fromS) (read toS)
    extract _ = error $ "Invalid move description: " <> T.unpack t

executeMove1 :: MoveExecutor
executeMove1 (Move amount from to) = replicateM_ amount executeSingle
  where
    executeSingle :: State Stacks ()
    executeSingle = do
      stacks <- get
      let fromStack = stacks ! from
      let toStack = stacks ! to
      let (toMove : newFromStack) = fromStack
      let newToStack = toMove : toStack
      let newStacks = (IM.insert from newFromStack . IM.insert to newToStack) stacks
      put newStacks

executeMove2 :: MoveExecutor
executeMove2 (Move amount from to) = do
  stacks <- get
  let fromStack = stacks ! from
  let toStack = stacks ! to
  let toMove = take amount fromStack
  let newFromStack = drop amount fromStack
  let newToStack = toMove ++ toStack
  let newStacks = (IM.insert from newFromStack . IM.insert to newToStack) stacks
  put newStacks

processInput :: MoveExecutor -> Input -> Stacks
processInput executeMove (Input stacks moves) = execState (traverse executeMove moves) stacks

testData' :: ByteString
testData' = $(embedFile "data/test/5-Stacks.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/5-Stacks.txt")