module HillClimbing (challengePair) where

import Data.ByteString (ByteString)
import Data.Char (ord)
import Data.FileEmbed (embedFile)
import Data.List (foldl', find)
import Data.Map (Map)
import qualified Data.Map as M
import Data.Maybe (fromJust)
import Data.Sequence (Seq(..), (|>))
import qualified Data.Sequence as Seq
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair solve1' solve2' testData' challengeData'

solve1' :: ByteString -> String
solve1' = show . shortestPath . buildGraph . parseGrid

solve2' :: ByteString -> String
solve2' = show . minimum . filter (/= 0) . fmap shortestPath . shuffleStart . buildGraph . parseGrid

type Height = Int
data SquareType = Regular | Start | End deriving (Eq, Show)
data Square = Square { height :: Height, squareType :: SquareType } deriving (Eq, Show)
type Grid = Map (Int, Int) Square

parseGrid :: ByteString -> Grid
parseGrid = parse . decodeUtf8
  where
    parse :: Text -> Grid
    parse = foldl' parseRow M.empty . zip [1..] . T.lines

    parseRow :: Grid -> (Int, Text) -> Grid
    parseRow grid (i, row) = foldl' parseSquare grid $ zip3 (repeat i) [1..] (T.unpack row)

    parseSquare :: Grid -> (Int, Int, Char) -> Grid
    parseSquare grid (i, j, c) = M.insert (i, j) (getSquare c) grid

    getSquare :: Char -> Square
    getSquare 'S' = Square (ord 'a') Start
    getSquare 'E' = Square (ord 'z') End
    getSquare c = Square (ord c) Regular

data Node = Node Square [(Int, Int)] deriving (Eq, Show)
data Graph = Graph { start :: (Int, Int), end :: (Int, Int), nodes :: Map (Int, Int) Node } deriving (Eq, Show)

buildGraph :: Grid -> Graph
buildGraph grid = Graph getStart getEnd buildNodes
  where
    getStart :: (Int, Int)
    getStart = fst $ fromJust $ find (\ass -> squareType (snd ass) == Start) (M.assocs grid)

    getEnd :: (Int, Int)
    getEnd = fst $ fromJust $ find (\ass -> squareType (snd ass) == End) (M.assocs grid)

    buildNodes :: Map (Int, Int) Node
    buildNodes = foldl' buildNode M.empty (M.assocs grid)

    buildNode :: Map (Int, Int) Node -> ((Int, Int), Square) -> Map (Int, Int) Node
    buildNode nodeMap ((i, j), square) = M.insert (i, j) (Node square (edges square i j)) nodeMap

    edges :: Square -> Int -> Int -> [(Int, Int)]
    edges square i j = filter (isEdge square) [(i - 1, j), (i, j + 1), (i + 1, j), (i, j - 1)]

    isEdge :: Square -> (Int, Int) -> Bool
    isEdge square (i', j') = maybe False (\neighbour -> neighbour.height <= square.height + 1) $ M.lookup (i', j') grid

shuffleStart :: Graph -> [Graph]
shuffleStart graph = (\newStart -> graph { start = newStart }) <$> possibleStarts
  where
    possibleStarts :: [(Int, Int)]
    possibleStarts = M.keys $ M.filter (\(Node (Square h _) _) -> h == ord 'a') graph.nodes

type Visited = Set (Int, Int)
type Distance = Int
type Queue = Seq (Distance, (Int, Int))
data State = State { queue :: Queue, visited :: Visited }

initialState :: (Int, Int) -> State
initialState startNode = State (Seq.singleton (0, startNode)) Set.empty

shortestPath :: Graph -> Int
shortestPath graph = go $ initialState graph.start
  where
    go :: State -> Int
    go state =
      case state.queue of
        Seq.Empty -> 0
        (distance, next) :<| rest ->
          if next == graph.end then
            distance
          else if next `Set.member` state.visited then
            let newState = state { queue = rest }
            in  go newState
          else
            let newQueue = foldl' (|>) rest (newNeighbours state.visited distance next)
                newState = state { queue = newQueue, visited = Set.insert next state.visited }
            in  go newState 

    newNeighbours :: Visited -> Distance -> (Int, Int) -> [(Distance, (Int, Int))]
    newNeighbours visited distance idx =
      let Node _ neighbours = fromJust $ M.lookup idx graph.nodes
      in  (distance + 1,) <$> filter (not . (`Set.member` visited)) neighbours

testData' :: ByteString
testData' = $(embedFile "data/test/12-Hills.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/12-Hills.txt")