module Monkeys (challengePair) where

import Control.Arrow ((>>>))
import Control.Monad (void)
import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.IntMap (IntMap)
import qualified Data.IntMap as IM
import Data.List (foldl', sort)
import Data.Maybe (fromJust)
import Data.Sequence (Seq, (|>))
import qualified Data.Sequence as Seq
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.Void (Void)
import Text.Megaparsec
import Text.Megaparsec.Char
import Challenge

challengePair :: ChallengePair
challengePair = ChallengePair (solve' 20 3) (solve' 10000 1) testData' challengeData'

solve' :: Int -> Int -> ByteString -> String
solve' rounds worryDiv =
  parseInput
  >>> runNTimes rounds (executeRound worryDiv)
  >>> IM.elems
  >>> fmap itemsInspected
  >>> sort
  >>> reverse
  >>> take 2
  >>> product
  >>> show
  where
    runNTimes :: Int -> (a -> a) -> a -> a
    runNTimes n f initial = foldl' (\result _ -> f result) initial [1..n]

type Parser = Parsec Void Text

data Test = Test { divisor :: Int, ifTrue :: Int, ifFalse :: Int } deriving (Eq, Show)
data Arg = Old | Lit Int deriving (Eq, Show)
data Op = Sum Arg Arg | Prod Arg Arg deriving (Eq, Show)
type Level = Integer
data Monkey = Monkey { idx :: Int, itemsInspected :: Int, items :: Seq Level, op :: Op, test :: Test } deriving (Eq, Show)
type Monkeys = IntMap Monkey

parseInput :: ByteString -> Monkeys
parseInput = either (error "Bad parse!") id . runParser inputParser "" . decodeUtf8

inputParser :: Parser Monkeys
inputParser = IM.fromList . zip [0..] <$> many monkeyParser
  where
    monkeyParser :: Parser Monkey
    monkeyParser = do
      idx <- string "Monkey" *> space *> (read <$> some digitChar) <* char ':' <* linebreaks
      items <- Seq.fromList <$> itemsParser
      op <- opParser
      test <- testParser
      linebreaks
      let itemsInspected = 0
      return $ Monkey { .. }

    itemsParser :: Parser [Level]
    itemsParser = space *> string "Starting items:" *> space *> some itemParser <* linebreaks

    itemParser :: Parser Level
    itemParser = (read <$> some digitChar) <* optional (char ',') <* space

    opParser :: Parser Op
    opParser = space *> string "Operation:" *> space *> string "new" *> space *> char '=' *> space
      *> (try (opParserWith '+' Sum) <|> opParserWith '*' Prod)
      <* linebreaks

    opParserWith :: Char -> (Arg -> Arg -> Op) -> Parser Op
    opParserWith c constructor = constructor <$> (argParser <* space <* char c <* space) <*> argParser

    argParser :: Parser Arg
    argParser = try (Old <$ string "old") <|> (Lit . read <$> some digitChar)

    testParser :: Parser Test
    testParser =
      Test <$> (space *> string "Test: divisible by " *> (read <$> some digitChar) <* linebreaks)
           <*> (space *> string "If true:" *> space *> string "throw to monkey " *> (read <$> some digitChar))
           <*> (space *> string "If false:" *> space *> string "throw to monkey " *> (read <$> some digitChar))

    linebreaks :: Parser ()
    linebreaks = void <$> many $ char '\n'

executeRound :: Int -> Monkeys -> Monkeys
executeRound worryDiv monkeys = foldl' (executeTurn worryDiv) monkeys (IM.keys monkeys)

executeTurn :: Int -> Monkeys -> Int -> Monkeys
executeTurn worryDiv monkeys monkeyIdx = clearItems $ addInspected $ foldl' (inspectItem monkey worryDiv) monkeys monkey.items
  where
    monkey :: Monkey
    monkey = fromJust $ IM.lookup monkeyIdx monkeys

    clearItems :: Monkeys -> Monkeys
    clearItems = IM.adjust (\m -> m { items = Seq.empty }) monkey.idx

    addInspected :: Monkeys -> Monkeys
    addInspected = IM.adjust (\m -> m { itemsInspected = m.itemsInspected + length monkey.items }) monkey.idx

inspectItem :: Monkey -> Int -> Monkeys -> Level -> Monkeys
inspectItem monkey worryDiv monkeys level =
  let newLevel = (runOp monkey.op level `div` fromIntegral worryDiv) `mod` fromIntegral commonModulo
      newMonkeyIdx = if newLevel `mod` fromIntegral monkey.test.divisor == 0 then monkey.test.ifTrue else monkey.test.ifFalse
      newMonkey = fromJust $ IM.lookup newMonkeyIdx monkeys
      newMonkey' = newMonkey { items = newMonkey.items |> newLevel }
  in  IM.insert newMonkeyIdx newMonkey' monkeys
  where
    commonModulo :: Int
    commonModulo = product $ divisor . test <$> monkeys

    runOp :: Op -> Level -> Level
    runOp (Sum arg1 arg2) = evalWith (+) arg1 arg2
    runOp (Prod arg1 arg2) = evalWith (*) arg1 arg2

    evalWith :: (Level -> Level -> Level) -> Arg -> Arg -> Level -> Level
    evalWith f arg1 arg2 old = f (evalArg arg1 old) (evalArg arg2 old)

    evalArg :: Arg -> Level -> Level
    evalArg Old old = old
    evalArg (Lit n) _ = fromIntegral n

testData' :: ByteString
testData' = $(embedFile "data/test/11-Monkeys.txt")

challengeData' :: ByteString
challengeData' = $(embedFile "data/challenge/11-Monkeys.txt")