module Main (main) where

import System.Environment (getArgs)
import Challenge
import qualified CalorieCounting
import qualified RockPaperScissors
import qualified Rucksack
import qualified Cleanup
import qualified Stacks
import qualified Tuning
import qualified NoSpaceLeft
import qualified Treehouse
import qualified RopeBridge
import qualified CathodeRayTube
import qualified Monkeys
import qualified HillClimbing
import qualified DistressSignal
import qualified Reservoir
import qualified BeaconExclusion

main :: IO ()
main = do
  [challengeString, mode] <- getArgs
  let challenge = selectChallenge challengeString
  
  case mode of
    "test" -> putStrLn $ challenge.solve challenge.testData
    "challenge" -> putStrLn $ challenge.solve challenge.challengeData
    _ -> error $ "invalid mode " <> mode

selectChallenge :: String -> Challenge
selectChallenge "1.1" = getPart CalorieCounting.challengePair Part1
selectChallenge "1.2" = getPart CalorieCounting.challengePair Part2
selectChallenge "2.1" = getPart RockPaperScissors.challengePair Part1
selectChallenge "2.2" = getPart RockPaperScissors.challengePair Part2
selectChallenge "3.1" = getPart Rucksack.challengePair Part1
selectChallenge "3.2" = getPart Rucksack.challengePair Part2
selectChallenge "4.1" = getPart Cleanup.challengePair Part1
selectChallenge "4.2" = getPart Cleanup.challengePair Part2
selectChallenge "5.1" = getPart Stacks.challengePair Part1
selectChallenge "5.2" = getPart Stacks.challengePair Part2
selectChallenge "6.1" = getPart Tuning.challengePair Part1
selectChallenge "6.2" = getPart Tuning.challengePair Part2
selectChallenge "7.1" = getPart NoSpaceLeft.challengePair Part1
selectChallenge "7.2" = getPart NoSpaceLeft.challengePair Part2
selectChallenge "8.1" = getPart Treehouse.challengePair Part1
selectChallenge "8.2" = getPart Treehouse.challengePair Part2
selectChallenge "9.1" = getPart RopeBridge.challengePair Part1
selectChallenge "9.2" = getPart RopeBridge.challengePair Part2
selectChallenge "10.1" = getPart CathodeRayTube.challengePair Part1
selectChallenge "10.2" = getPart CathodeRayTube.challengePair Part2
selectChallenge "11.1" = getPart Monkeys.challengePair Part1
selectChallenge "11.2" = getPart Monkeys.challengePair Part2
selectChallenge "12.1" = getPart HillClimbing.challengePair Part1
selectChallenge "12.2" = getPart HillClimbing.challengePair Part2
selectChallenge "13.1" = getPart DistressSignal.challengePair Part1
selectChallenge "13.2" = getPart DistressSignal.challengePair Part2
selectChallenge "14.1" = getPart Reservoir.challengePair Part1
selectChallenge "14.2" = getPart Reservoir.challengePair Part2
selectChallenge "15.1" = getPart BeaconExclusion.challengePair Part1
selectChallenge "15.2" = getPart BeaconExclusion.challengePair Part2
selectChallenge s = error $ "invalid challenge: " <> s